#!/bin/bash

# centosからcloud-initで起動することを想定したスクリプトです
# コピーとかして使ってください


cd /root
git clone "https://sakaki333@bitbucket.org/sakaki333/chef_test.git"
cp -r chef_test/apache chef-repo/cookbooks
cp chef_test/test.json chef-repo/environments

cd chef-repo
chef exec chef-client --local-mode -j environments/test.json
